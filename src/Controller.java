import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Controller implements IController,
        ConvertorMoney.CallBack, ConvertorValue.Callback,  Calc.CallBack {
    private IFactory factory;
    private BufferedReader reader;

    public Controller() {
        reader = new BufferedReader(new InputStreamReader(System.in));
    }

    @Override
    public void run() {
        String cmd = null;
        try {
            System.out.println("Выбирите что вы хотите запустить"
                    + "\n" + "1. Калькулятор(пока не работет)"
                    + "\n" + "2. Конвертер валют"
                    + "\n" + "3. Конвертер величин");


            int select = Integer.parseInt(reader.readLine());
            switch (select) {
                case 1:
                    cmd = Const.TAG_CALCK;
                    break;
                case 2:
                    cmd = Const.TAG_CONVERTOR_CURRENCY;
                    break;
                case 3:
                    cmd = Const.TAG_CONVERTOR_VALUE;
                    break;
                default:
                    cmd = Const.MESSAGE_NOT_NUMB;
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (NumberFormatException e) {
            System.out.println(Const.MESSAGE_NOT_FOMAT + "\n");
            run();
        }
        if (cmd == null) {
            System.out.println(Const.ERROR);
            run();
        }
        if (cmd == Const.MESSAGE_NOT_NUMB) {
            System.out.println(Const.MESSAGE_NOT_NUMB + "\n");
            run();
        }
        try {
            IFactory factory = Factory.getInstance().factoryMethod(this, this, cmd);
        } catch (Exception e) {
            run();
        }


    }


    @Override
    public void callBack(String value) {
        System.out.println(
                Const.RESULT
                .concat(" ")
                .concat(value));

    }

    @Override
    public void callback(String value) {
        System.out.println(Const.RESULT.concat(" ").concat(value));
    }
}
