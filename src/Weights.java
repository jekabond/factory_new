import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Weights {
    private BufferedReader reader;
    private String weight = null;

    public String weights() {
        int sel1 = 0;
        int sel2 = 0;
        reader = new BufferedReader(new InputStreamReader(System.in));
        String weight = "ваш вес";
        System.out.println("Что вы хотите перевести"
                + "\n"
                + "1. Граммы"
                + "\n"
                + "2. Килограммы"
                + "\n"
                + "3. Тонна"
                + "\n"
                + "4. Фунты"
                + "\n"
                + "5. Унция");
        try {
            sel1 = Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Во что вы хотите перевести"
                + "\n"
                + "1. Граммы"
                + "\n"
                + "2. Килограммы"
                + "\n"
                + "3. Тонна"
                + "\n"
                + "4. Фунты"
                + "\n"
                + "5. Унция");
        try {
            sel2 = Integer.parseInt(reader.readLine());
        } catch (IOException e) {
            e.printStackTrace();
        }
        switch (sel1) {
            case 1: {
                weight = gramm(sel2);
            }
            break;
            case 2: {
                weight = killoGram(sel2);
            }
            break;
            case 3: {
                weight = tonn(sel2);
            }
            break;
            case 4: {
                weight = funt(sel2);
            }
            break;
            case 5: {
                weight = unts(sel2);
            }
            break;
        }
        return weight;
    }

    public Object vvodChisla() {
        try {
            return Long.parseLong(reader.readLine());
        } catch (IOException e) {
            try {
                return Double.parseDouble(reader.readLine());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } catch (NumberFormatException e) {
            System.out.println(Const.MESSAGE_NOT_FOMAT);
        }
        return -1;
    }

    public String gramm(int sel2) {
        switch (sel2) {
            case 1: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue));
                weight = result1;
            }
            break;
            case 2: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 0.001);
                weight = result1;
            }
            break;
            case 3: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 0.000001);
                weight = result1;
            }
            break;
            case 4: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 0.00220462);
                weight = result1;
            }
            break;
            case 5: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 0.035274);
                weight = result1;

            }
            break;

        }
        return weight;
    }

    public String killoGram(int sel2) {
        switch (sel2) {
            case 1: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 1000);
                weight = result1;
            }
            break;
            case 2: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue));
                weight = result1;
            }
            break;
            case 3: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 0.001);
                weight = result1;
            }
            break;
            case 4: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 2.20462);
                weight = result1;
            }
            break;
            case 5: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 35.274);
                weight = result1;

            }
            break;

        }
        return weight;
    }

    public String tonn(int sel2) {
        switch (sel2) {
            case 1: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 1000000);
                weight = result1;
            }
            break;
            case 2: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 1000);
                weight = result1;
            }
            break;
            case 3: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue));
                weight = result1;
            }
            break;
            case 4: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 2204.62);
                weight = result1;
            }
            break;
            case 5: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 35274);
                weight = result1;

            }
            break;

        }
        return weight;
    }

    public String funt(int sel2) {
        switch (sel2) {
            case 1: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 453.592);
                weight = result1;
            }
            break;
            case 2: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 0.453592);
                weight = result1;
            }
            break;
            case 3: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 0.000453592);
                weight = result1;
            }
            break;
            case 4: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue));
                weight = result1;
            }
            break;
            case 5: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 16);
                weight = result1;

            }
            break;

        }
        return weight;
    }

    public String unts(int sel2) {
        switch (sel2) {
            case 1: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 28.3495);
                weight = result1;
            }
            break;
            case 2: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 0.0283495);
                weight = result1;
            }
            break;
            case 3: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 0.0000283495);
                weight = result1;
            }
            break;
            case 4: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue) * 0.0625);
                weight = result1;
            }
            break;
            case 5: {
                System.out.println(Const.INPUT_VAL);
                String inputValue = String.valueOf(vvodChisla());
                String result1 = String.valueOf(Long.parseLong(inputValue));
                weight = result1;

            }
            break;

        }
        return weight;
    }
}
