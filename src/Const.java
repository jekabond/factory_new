
abstract class Const {
     static final String TAG_CALCK = "calck";
     static final String TAG_CONVERTOR_CURRENCY = "convertor";
     static final String TAG_CONVERTOR_VALUE = "convertorValue";
     static final String MESSAGE_NOT_FOMAT = "Вы использовали не правельный формат ввода. Сделайте выбор снова";
     static final String MESSAGE_NOT_NUMB = "Вы ввели не верное число. Сделайте выбор снова";
     static final String ERROR = "Произошла ошибка. Повторите свой выбор снова";
     static final String RESULT = "Ваш результат = ";
     static final String NEGATIVE_MEANING = "Вы ввели отрицательную сумму. Пожалуйста исправте ошибку";
     static final String EXIT = "Выход из программы";
     static final String MAIN_MENU = "Выход в гравное меню";
     static final String INPUT_VAL = "Введите значение: ";
}
