import com.sun.xml.internal.ws.api.ha.StickyFeature;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Калькулятор
 * операци + , - , * , /
 *
 * @param <T>
 */
public class Calc<T extends Calc.CallBack, C extends IController> implements IFactory {
    private T callback;
    private C controller;
    private BufferedReader reader;
    private boolean flagRaboti;
    private String oper = null;

    interface CallBack {
        void callBack(String value);
    }

    public Calc(T callback, C controller) {
        this.callback = callback;
        this.controller = controller;
        reader = new BufferedReader(new InputStreamReader(System.in));
        workcalc();
    }

    public void workcalc() {
        do {
            System.out.println("Добро пожаловать в калькулятор"
                    + "\n"
                    + "Выбирите операцию"
                    + "\n"
                    + "| + | - | * | / | или используйте help - для вызова меню");
            try {
                oper = reader.readLine();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                System.out.println(Const.MESSAGE_NOT_FOMAT);
            }
//            if (oper == null) {
//                callback.callBack(Const.ERROR);
//            }
//            workcalc();
            if (oper != null) {
                help(oper);
                getValue("0");
            }
        } while (flagRaboti);

    }

    public void help(String oper) {
        if (oper == null) {
            System.out.println(Const.ERROR);
        }
        if (oper.equals("help")) {
            System.out.println("Сделайте свой выбор:"
                    + "\n" + "1. " + Const.MAIN_MENU
                    + "\n" + "2. " + Const.EXIT);
            try {
                int sel = Integer.parseInt(reader.readLine());
                if (sel == 1) {
                    controller.run();
                    return;
                }
                if (sel == 2) {
                    System.exit(0);
                }
                if (sel > 2) {
                    System.out.println(Const.MESSAGE_NOT_FOMAT + "\n");
                    help("help");
                }
            } catch (IOException e) {
                e.printStackTrace();
            } catch (NumberFormatException e) {
                System.out.println(Const.MESSAGE_NOT_FOMAT + "\n");
                help("help");
            }
        }
//        if (oper != null){
//            caclOper(oper);
//        }
    }

    public void getValue(String sum) {
        System.out.println("Введите первое число");
        Object a = chislovvoda();
        System.out.println("Введите первое число");
        Object b = chislovvoda();
//        Processing().sum();
        String result = sum;
        callback.callBack(sum);

    }

    public Object chislovvoda() {
        try {
            return Long.parseLong(reader.readLine());
        } catch (IOException e) {
            try {
                return Double.parseDouble(reader.readLine());
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        } catch (NumberFormatException e) {
            System.out.println(Const.MESSAGE_NOT_FOMAT);
        }
        return -1;
    }
}
//    public void caclOper (String oper){
//        if (oper.equals("+")); new Processing().sum(oper);
//
//        if (oper.equals("-"));
//
//
//    }


