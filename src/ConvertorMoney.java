import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * Должен конвертировать
 * с доллоров в гривну  parm 1
 * с гривни в доллоры   parm 2
 * курс 27.25
 */
public class ConvertorMoney <T extends ConvertorMoney.CallBack, C extends IController> implements IFactory {
    private T callback;
    private C controller;
    private BufferedReader reader;
    private boolean flagWorking = true;
    private String znachenie = null;
    private double meaning = 0;

    interface CallBack {
        void callBack(String value);
    }

    public ConvertorMoney(T callback, C controller) {
        this.callback = callback;
        this.controller = controller;
        reader = new BufferedReader(new InputStreamReader(System.in));
        workConvertor();
    }

    public void workConvertor() {
        do {

            System.out.println("Добро пожаловать в конвертер валют"
                    + "\n" + "\n"
                    + "Выбор конвертации валюты по курсу 27,25. Для вызова Help введите 3"
                    + "\n" + "1. USD -> UAH"
                    + "\n" + "2. UAH -> USD");
            int select = 0;
            try {
                select = Integer.parseInt(reader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }

            switch (select) {
                case 1:
                    System.out.println("Введите количество USD");
                    double var = chislovvoda();
                    if (var == -1) {
                        workConvertor();
                    }
                    if (var < 0) {
                        System.out.println(Const.NEGATIVE_MEANING);
                        var = chislovvoda();
                    }
                    meaning = var * 27.25;
                    znachenie = Double.toString(meaning);
                    callback.callBack(znachenie);
                    break;
                case 2:
                    System.out.println("Введите количество UAH");
                    var = chislovvoda();
                    if (var == -1) {
                        workConvertor();
                    }
                    if (var < 0) {
                        System.out.println(Const.NEGATIVE_MEANING);
                        var = chislovvoda();
                    }
                    meaning = var / 27.25;
                    znachenie = Double.toString(meaning);
                    callback.callBack(znachenie);
                    break;
                case 3:
                    System.out.println("Сделайте свой выбор:"
                            + "\n" + "1. " + Const.MAIN_MENU
                            + "\n" + "2. " + Const.EXIT);
                    try {
                        int sel = Integer.parseInt(reader.readLine());
                        if (sel == 1) {
                            controller.run();
                            return;
                        }
                        if (sel == 2) {
                            System.exit(0);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                default:
                    System.out.println(Const.MESSAGE_NOT_FOMAT + "\n");
                    workConvertor();
                    break;
            }

        } while (flagWorking);

    }

    public double chislovvoda() {
        try {
            return Double.parseDouble(reader.readLine());
        } catch (IOException e) {
        } catch (NumberFormatException e) {
            System.out.println(Const.MESSAGE_NOT_FOMAT);
        }
        return -1;
    }
}
