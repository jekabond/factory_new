import java.io.BufferedReader;
        import java.io.IOException;
        import java.io.InputStreamReader;

public class ConvertorValue<T extends ConvertorValue.Callback, C extends IController> implements IFactory {
    private T callback;
    private C controller;
    private BufferedReader reader;
    private boolean flagWork = true;

    interface Callback {
        void callback(String value);
    }

    public ConvertorValue(T callback, C controller) {
        this.callback = callback;
        this.controller = controller;
        reader = new BufferedReader(new InputStreamReader(System.in));
        convertVal();
    }

    public void convertVal() {
        int sel = 0;
        String value = null;
        do {
            System.out.println("Что вы хотите преобразовать"
                    + "\n"
                    + "1. Меры длины"
                    + "\n"
                    + "2. Меры объема"
                    + "\n"
                    + "3. Меры веса"
                    + "\n"
                    + "4. Вызов меню");
            try {
                sel = Integer.parseInt(reader.readLine());
            } catch (IOException e) {
                e.printStackTrace();
            }
            switch (sel) {
                case 1:
                    value = new Lengths().lengths();
                    callback.callback(value);
                    break;
                case 2:
                    value = new Volumes().volumes();
                    callback.callback(value);
                    break;
                case 3:
                    value = new Weights().weights();
                    callback.callback(value);
                    break;
            }


        } while (flagWork);
    }
}
