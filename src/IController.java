import java.io.IOException;

public interface IController {
    void run();
}
